from django.core.management.base import BaseCommand
import sys
import logging
import json
from backend.export import export_db

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Export all the NM database'

    def add_arguments(self, parser):
        parser.add_argument("--quiet", action="store_true", default=None,
                            help="Disable progress reporting")
        parser.add_argument("--full", action="store_true", default=None,
                            help="Also export privacy-sensitive information")

    def handle(self, quiet=False, full=False, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if quiet:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        exported = export_db(full)

        class Serializer(json.JSONEncoder):
            def default(self, o):
                if hasattr(o, "strftime"):
                    return o.strftime("%Y-%m-%d %H:%M:%S")
                return json.JSONEncoder.default(self, o)

        json.dump(exported, sys.stdout, cls=Serializer, indent=2)
