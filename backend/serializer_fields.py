from rest_framework import serializers
from .models import Person


class PersonKeyField(serializers.Field):
    def __init__(self, *args, **kw):
        many = kw.pop("many", False)
        super().__init__(*args, **kw)
        self.many = many

    def to_representation(self, value):
        if self.many:
            return [x.lookup_key for x in value.all()]
        else:
            return value.lookup_key

    def lookup_person_key(self, key):
        if self.context:
            importer = self.context.get("importer")
            if importer:
                return importer.people[key]
        else:
            return Person.lookup(key)

    def to_internal_value(self, data):
        return data


class AMKeyField(serializers.Field):
    def to_representation(self, value):
        return value.person.lookup_key

    def to_internal_value(self, data):
        return data
