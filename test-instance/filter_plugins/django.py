import random


def secret_key(value):
    """
    Generate a Django secret key
    """
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    generator = random.SystemRandom()
    return ''.join(generator.choice(chars) for i in range(50))


class FilterModule(object):
    '''
    custom jinja2 filters for working with collections
    '''

    def filters(self):
        return {
            'django_secret_key': secret_key
        }
