from __future__ import annotations
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.views.generic import View, TemplateView
from django.contrib.auth import get_user_model
from django import http
from django.contrib import auth
from django.utils.module_loading import import_string
from django.shortcuts import redirect, get_object_or_404
from .models import Identity
from . import providers


SIGNON_VIEW_MIXIN = getattr(settings, "SIGNON_VIEW_MIXIN", None)
if SIGNON_VIEW_MIXIN:
    SignonMixin = import_string(SIGNON_VIEW_MIXIN)
else:
    SignonMixin = None


def make_template_view(name, base):
    if SignonMixin is None:
        return type(name, (base, TemplateView), {})
    else:
        return type(name, (base, SignonMixin, TemplateView), {})


class LoginMixin:
    """
    Mixin to plug into a TemplateView to handle logins
    """
    template_name = "signon/login.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        active = []
        inactive = []
        for provider in (x.bind(self.request) for x in getattr(settings, "SIGNON_PROVIDERS", ())):
            if provider.get_active_identity() is not None:
                active.append(provider)
            else:
                inactive.append(provider)
        ctx["providers_active"] = active
        ctx["providers_inactive"] = inactive
        if self.request.user.is_authenticated:
            ctx["identities"] = self.request.user.identities.all()
        return ctx


Login = make_template_view("Login", LoginMixin)


class Logout(View):
    def post(self, request, *args, **kw):
        provider = self.kwargs.get("name")

        if provider is None:
            # Remove all Identities currently in session
            for provider in getattr(settings, "SIGNON_PROVIDERS", ()):
                provider.logout(request)
        else:
            providers.get(provider).logout(request)

        if request.user.is_authenticated and not self.request.signon_identities:
            # Log out the Django user if they were logged in and no active
            # signon identities are left
            auth.logout(request)

        next_url = request.POST.get("next")
        if next_url is None:
            return redirect(getattr(settings, "SIGNON_DEFAULT_REDIRECT", "home"))
        else:
            return redirect(next_url)


class Whoami(View):
    def get(self, request, *args, **kw):
        identities = {}
        for name, identity in request.signon_identities.items():
            identities[name] = {
                "issuer": identity.issuer,
                "subject": identity.subject,
                "last_used": identity.last_used.strftime("%Y-%M-%d"),
                "profile": identity.profile,
                "picture": identity.picture,
                "fullname": identity.fullname,
                "username": identity.username,
                "person": str(identity.person) if identity.person else None,
            }
        return http.JsonResponse({
            "user": {
                "pk": self.request.user.pk,
                "str": str(self.request.user),
            },
            "identities": identities,
        })


class BaseMixin:
    def _load_common_info(self):
        pass

    def get(self, request, *args, **kw):
        self._load_common_info()
        return super().get(request, *args, **kw)

    def post(self, request, *args, **kw):
        self._load_common_info()
        return super().post(request, *args, **kw)


class LinkMixin(BaseMixin):
    template_name = "signon/link.html"

    def _load_common_info(self):
        # Load the identity we need to link/relink
        self.identity = self.request.signon_identities.get(self.kwargs["provider"])
        if self.identity is None:
            raise PermissionDenied()

        # Load the target user
        self.target = get_object_or_404(get_user_model(), pk=self.kwargs["pk"])

        # Check if the target user is bound to one of the active identities
        for identity in self.request.signon_identities.values():
            if identity.person == self.target:
                break
        else:
            raise PermissionDenied()

        # Check if this is the last identity of its user
        if self.identity.person:
            self.is_last = list(self.identity.person.identities.all()) == [self.identity]
        else:
            self.is_last = False

        # Check if this user model supports merging users
        self.can_merge = hasattr(get_user_model(), "merge")

    def post(self, request, *args, **kw):
        self._load_common_info()
        if self.is_last and self.can_merge:
            self.target.merge(
                    self.identity.person,
                    audit_author=self.target, audit_notes="Merged by linking via logins interface")
        else:
            self.identity.person = self.target
            self.identity.save(audit_author=self.target, audit_notes="Linked via logins interface")
        return redirect("signon:login")

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["provider"] = providers.get(self.identity.issuer)
        ctx["identity"] = self.identity
        ctx["target"] = self.target
        ctx["is_last"] = self.is_last
        ctx["can_merge"] = self.can_merge
        return ctx


Link = make_template_view("Link", LinkMixin)


class UnlinkMixin:
    template_name = "signon/unlink.html"

    def post(self, request, *args, **kw):
        identity = self.request.signon_identities.get(self.kwargs["provider"])
        if identity is None:
            raise http.Http404()

        person = identity.person
        identity.person = None
        identity.save(audit_author=person, audit_notes="Unlinked via logins interface")
        return redirect("signon:login")

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        identity = self.request.signon_identities.get(self.kwargs["provider"])
        if identity is None:
            raise http.Http404()
        ctx["provider"] = providers.get(self.kwargs["provider"], None)
        ctx["identity"] = identity
        if identity.person:
            is_last = list(identity.person.identities.all()) == [identity]
        else:
            is_last = False
        ctx["is_last"] = is_last
        return ctx


Unlink = make_template_view("Unlink", UnlinkMixin)


class OIDCAuthenticationCallbackView(View):
    def get(self, request, *args, **kw):
        User = get_user_model()
        name = self.kwargs["name"]
        provider = providers.get(name, None)
        if provider is None:
            raise http.Http404

        provider = provider.bind(request)
        provider.load_tokens()

        info = provider.get_userinfo()

        claims = provider.id_token_claims
        try:
            identity = Identity.objects.get(issuer=name, subject=claims["sub"])
        except Identity.DoesNotExist:
            identity = Identity.objects.create(
                    issuer=name, subject=claims["sub"],
                    audit_author=User.objects.get_housekeeper(),
                    audit_notes="Identity created automatically on successful OIDC authentication")

        changed = identity.update(
            profile=info.get("profile"),
            picture=info.get("picture"),
            fullname=info.get("name"),
            username=info.get("nickname"),
            audit_author=User.objects.get_housekeeper(),
            audit_notes="updated from login user information")

        if not changed:
            # Save to update last_used
            identity.save(audit_skip=True)

        request.session[f"signon_identity_{name}"] = identity.pk
        return redirect(getattr(settings, "SIGNON_DEFAULT_REDIRECT", "home"))
