"""
Django settings for nm project.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

from django.utils.translation import gettext_lazy as _
from nm2.lib.log_utils import get_logging_config
from signon import providers
import os.path
import datetime
import sys

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# BASE_DIR is the default from newer djangos. PROJECT_DIR could still be used
# in developers settings, so it's kept here for a while
PROJECT_DIR = BASE_DIR

DATA_DIR = os.path.join(BASE_DIR, 'data')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'thisisnotreallysecretweonlyuseitfortestingharrharr'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

TESTING = 'test' in sys.argv

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    # https://github.com/spanezz/django-housekeeping
    'django_housekeeping',
    'rest_framework',
    'legacy',
    'keyring',
    'dsa',
    'deblayout',
    'nmlayout',
    'backend',
    'apikeys',
    'person',
    'public',
    'restricted',
    'process',
    'fprs',
    'dm',
    'maintenance',
    'projectb',
    'minechangelogs',
    'api',
    'contributors',
    'wizard',
    'mia',
    'sitechecks',
    'deploy',
    'signon',
    'impersonate',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # Needs to stay right after AuthenticationMiddleware
    'signon.middleware.SignonMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'impersonate.middleware.ImpersonateMiddleware',
]

AUTHENTICATION_BACKENDS = [
    'signon.middleware.SignonAuthBackend',
]

ROOT_URLCONF = 'nm2.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            "./", "templates"
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'backend.context_processors.const',
            ],
        },
    },
]

# Database configuration for development environments
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
         # Or path to database file if using sqlite3.
        'NAME': '%s/db-used-for-development.sqlite' % DATA_DIR,
    },
    'projectb': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'projectb',
        'USER': 'guest',
        'HOST': 'localhost',
        # ssh mirror.ftp-master.debian.org -N -L15434:bmdb1.debian.org:5434
        'PORT': '15434',                 # Port forwarded
        # 'PORT': '5433',                  # Local
    },
}
if TESTING:
    DATABASES["projectb"] = {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': '{}/test-projectb.sqlite'.format(DATA_DIR),
    }

# Prevent attempts to create tables on projectb (they would fail anyway)
DATABASE_ROUTERS = ["projectb.router.DbRouter"]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'

# Enable using system assets
from django.conf.global_settings import STATICFILES_DIRS # noqa
STATICFILES_DIRS += [
    ("common", "/usr/share/javascript/"),
]


ADMINS = (
    ('Debian New Member Front Desk', 'nm@debian.org'),
)
MANAGERS = ADMINS

DEFAULT_FROM_EMAIL = "nm@debian.org"
SERVER_EMAIL = "nm@debian.org"

LOCALE_PATHS = [os.path.join(PROJECT_DIR, "locale")]

LANGUAGES = (
    ('de', _('German')),
    ('en', _('English')),
    ('es', _('Spanish')),
    ('it', _('Italian')),
    ('fr', _('French')),
)


SITE_ID = 1


AUTH_USER_MODEL = 'backend.Person'


REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'apikeys.authentication.ApiKeyAuthentication',
    ),
}


# LDAP server to use to access Debian's official LDAP information
LDAP_SERVER = "ldap://db.debian.org"

# Location of temporary keyrings used by keycheck
KEYRINGS_TMPDIR = os.path.join(DATA_DIR, "tmp_keyrings")

# Keyring used to validate signatures of keyring-maint members
KEYRING_MAINT_KEYRING = os.path.join(DATA_DIR, "keyring-maint.gpg")

# Git repository of keyring-maint's git repo
KEYRING_MAINT_GIT_REPO = os.path.join(DATA_DIR, "keyring-maint.git")

# Work paths used by minechangelogs (indexing cache and the index itself)
MINECHANGELOGS_CACHEDIR = os.path.join(DATA_DIR, "mc_cache")
MINECHANGELOGS_INDEXDIR = os.path.join(DATA_DIR, "mc_index")

# Directory where site backups are stored
HOUSEKEEPING_ROOT = os.path.join(DATA_DIR, "housekeeping")

# Directory where applicant mailboxes are stored
PROCESS_MAILBOX_DIR = os.path.join(DATA_DIR, "applicant-mailboxes")

# Directory where applicant mailboxes are stored for old-style processes
PROCESS_MAILBOX_DIR_OLD = os.path.join(DATA_DIR, "applicant-mailboxes")

# Date where we imported DM information that had no timestamp.
# DMs created on this date are in fact DMs created on an unknown date
DM_IMPORT_DATE = datetime.datetime(2012, 3, 14, tzinfo=datetime.timezone.utc)

# The password for this account is available from:
# master.debian.org:/home/debian/misc/rt-password
RT_LOGIN_INFO = {'user': "debian", 'pass': "the_guest_password"}

# Directory where email configuration is stored
MAIL_CONFDIR = "/srv/nm.debian.org/mail"

SIGNON_PROVIDERS = [
    providers.DebssoProvider(name="debsso", label="sso.debian.org"),
]

SIGNON_VIEW_MIXIN = "nmlayout.mixins.SignonMixin"

# Automatically bind unbound identifiers when active together with a bound one
SIGNON_AUTO_BIND = False

SIGNON_TEST_FIXTURE = "backend.unittest.SignonFixtureMixin"
IMPERSONATE_TEST_FIXTURE = "backend.unittest.ImpersonateFixtureMixin"

# Sync with this salsa host
SALSA_HOST = "https://salsa-test.debian.net"

# IPs to which we allow user information to be exported for Salsa
SALSA_EXPORT_ALLOW_IPS = []

# IPs to which we allow identity information to be exported for
# contributors.debian.org
IDENTITIES_EXPORT_ALLOW_IPS = []

# If set, the RT API calls when a process if approved won't be done and
# a mock id will be returned
MOCK_RT = False

# Mail address of the Community Team
COMMUNITY_EMAIL = "community@debian.org"

# Try importing local settings from local_settings.py, if we can't,
# it's just fine, use defaults from this file
try:
    from .local_settings import *  # noqa
except ImportError:
    pass

# Log to file on top of the normal django logging
LOGGING = get_logging_config(f"{DATA_DIR}/logs/django.log")
