from __future__ import annotations
from django.test import TestCase
from django.urls import reverse
from backend.unittest import PersonFixtureMixin
from backend import const
from signon.models import Identity


class NewProcTests(PersonFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        targets = ["dm", "ga", "return", "invalid"]
        targets.extend(const.ALL_STATUS_DESCS.keys())

        for target in targets:
            cls._add_method(cls._test_notlogged, target)
            cls._add_method(cls._test_active_unbound, target)

    def _test_notlogged(self, target):
        # Check that the new person is listed on the page
        client = self.make_test_client(None)
        response = client.get(reverse('wizard:newprocess', args=[target]))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "This wizard only works when you are logged into the site")

    def _test_active_unbound(self, target):
        # Check that the new person is listed on the page
        identity = Identity.objects.create(
                issuer="debsso", subject="test@debian.org",
                username="test@debian.org", audit_skip=True)
        client = self.make_test_client(None, [identity])
        response = client.get(reverse('wizard:newprocess', args=[target]))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "This wizard only works when you are logged into the site")

    def test_dc_to_dm(self):
        client = self.make_test_client(self.persons.dc)
        response = client.get(reverse('wizard:newprocess', args=["dm"]))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context["allowed"])
        self.assertContains(response, "and follow the instructions")

    def test_dcga_to_dm(self):
        client = self.make_test_client(self.persons.dc_ga)
        response = client.get(reverse('wizard:newprocess', args=["dm"]))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context["allowed"])
        self.assertContains(response, "and follow the instructions")

    def test_dm_to_dm(self):
        client = self.make_test_client(self.persons.dm)
        response = client.get(reverse('wizard:newprocess', args=["dm"]))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context["allowed"])
        self.assertNotContains(response, "and follow the instructions")
        self.assertContains(response, "You are already a Debian Maintainer")

    def test_dmga_to_dm(self):
        client = self.make_test_client(self.persons.dm_ga)
        response = client.get(reverse('wizard:newprocess', args=["dm"]))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context["allowed"])
        self.assertNotContains(response, "and follow the instructions")
        self.assertContains(response, "You are already a Debian Maintainer")

    def test_ddnu_to_dm(self):
        client = self.make_test_client(self.persons.dd_nu)
        response = client.get(reverse('wizard:newprocess', args=["dm"]))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context["allowed"])
        self.assertNotContains(response, "and follow the instructions")
        self.assertContains(response, "You are already")
