from __future__ import annotations
from django.test import TestCase
from django.urls import reverse
from backend.unittest import PersonFixtureMixin
from signon.models import Identity


class WhoamiTests(PersonFixtureMixin, TestCase):
    def test_notlogged(self):
        # Check that the new person is listed on the page
        client = self.make_test_client(None)
        response = client.get(reverse('wizard:home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "You are not currently logged in.")
        self.assertNotContains(response, "but not mapped to any person in the site.")
        self.assertNotContains(response, "You are currently logged in, and recognized as")

    def test_active_unbound(self):
        # Check that the new person is listed on the page
        identity = Identity.objects.create(
                issuer="debsso", subject="test@debian.org",
                username="test@debian.org", audit_skip=True)
        client = self.make_test_client(None, [identity])
        response = client.get(reverse('wizard:home'))
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, "You are not currently logged in.")
        self.assertContains(response, "but not mapped to any person in the site.")
        self.assertNotContains(response, "You are currently logged in, and recognized as")

    def test_active_bound(self):
        # Check that the new person is listed on the page
        identity = Identity.objects.create(
                person=self.persons.dc,
                issuer="debsso", subject="test@debian.org",
                username="test@debian.org", audit_skip=True)
        client = self.make_test_client(self.persons.dc, [identity])
        response = client.get(reverse('wizard:home'))
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, "You are not currently logged in.")
        self.assertNotContains(response, "but not mapped to any person in the site.")
        self.assertContains(response, "You are currently logged in, and recognized as")
