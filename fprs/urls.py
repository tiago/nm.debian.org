from __future__ import annotations
from django.urls import path, re_path
from . import views

app_name = "fprs"

urlpatterns = [
    # Manage fingerprints
    path('person/<key>/', views.PersonFingerprints.as_view(), name="person_list"),
    re_path('^person/(?P<key>[^/]+)/(?P<fpr>[0-9A-F]+)/endorsements/view/$', views.ViewKeyEndorsements.as_view(), name="view_endorsements"),
    re_path('^person/(?P<key>[^/]+)/(?P<fpr>[0-9A-F]+)/endorsements/add/$', views.EndorseKey.as_view(), name="endorse"),
    re_path('^person/(?P<key>[^/]+)/(?P<fpr>[0-9A-F]+)/endorsements/(?P<endorsement>[0-9]+)/raw/$', views.ViewRawKeyEndorsement.as_view(), name="view_raw_endorsement"),
    re_path('^person/(?P<key>[^/]+)/(?P<fpr>[0-9A-F]+)/endorsements/(?P<endorsement>[0-9]+)/delete/$', views.DeleteKeyEndorsement.as_view(), name="delete_endorsement"),
    # Activate fingerprints
    re_path(r'^person/(?P<key>[^/]+)/(?P<fpr>[0-9A-F]+)/activate/$', views.SetActiveFingerprint.as_view(),
            name="person_activate"),
]
