import backend.ops as op
from backend import models as bmodels

class FingerprintEndorsementField(op.OperationField):
    def validate(self, val):
        if val is None:
            return val
        if isinstance(val, bmodels.FingerprintEndorsement):
            return val
        else:
            return bmodels.FingerprintEndorsement.objects.get(pk=val)

    def to_json(self, val):
        if val is None:
            return val
        return val.pk


@op.Operation.register
class FingerprintEndorsementAdd(op.Operation):
    endorsement = op.StringField()
    fingerprint = op.FingerprintField()
    endorsing_fpr = op.FingerprintField()

    def __init__(self, **kw):
        kw.setdefault("audit_notes", "Added a new endorsement")
        self._fingerprint = kw.get('fingerprint', None)
        super().__init__(**kw)
        self._endorsement = None

    def _execute(self):
        self._endorsement = bmodels.FingerprintEndorsement(
            endorsing_fpr=self.audit_author.fingerprint,
            author=self.audit_author,
            text=self.endorsement,
            fingerprint=self._fingerprint,
        )
        self._endorsement.date = self.audit_time
        self._endorsement.save()

    def notify(self, request=None):
        from .email import notify_new_endorsement
        notify_new_endorsement(self._endorsement, request=request, date=self.audit_time, notify_ml="newmaint")


@op.Operation.register
class FingerprintEndorsementRemove(op.Operation):
    endorsement = FingerprintEndorsementField()

    def __init__(self, **kw):
        kw.setdefault("audit_notes", "Deleted endorsement")
        super().__init__(**kw)

    def _execute(self):
        self.endorsement.delete()
