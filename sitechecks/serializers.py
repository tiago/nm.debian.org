from rest_framework import serializers
from backend.serializer_fields import PersonKeyField
from . import models


class ProcessField(serializers.Field):
    def to_representation(self, value):
        return [value.applying_for, value.started]

    def to_internal_value(self, data):
        return data


class InconsistencyExportSerializer(serializers.ModelSerializer):
    person = PersonKeyField()
    process = ProcessField()

    class Meta:
        model = models.Inconsistency
        exclude = ["id"]
