"""
Test permissions
"""
from django.test import TestCase
from django.urls import reverse
from backend import const
from backend.unittest import PersonFixtureMixin, PageElements


class TestStatsPermissions(PersonFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.elements = PageElements()
        cls.elements.add_id("head_lastlog")
        cls.elements.add_class("col_lastlog")
        cls.processes.create("app", person=cls.persons.dm, applying_for=const.STATUS_DM_GA)

    @classmethod
    def __add_extra_tests__(cls):
        # Everyone can see stats
        for visitor in (None, "pending", "dc", "dc_ga", "dm", "dm_ga", "dd_nu", "dd_u", "dd_e", "dd_r", "oldam"):
            cls._add_method(cls._test_get_success, visitor, elements=())

        # But only AMs can see process-specific information
        for visitor in ("activeam", "fd", "dam"):
            cls._add_method(
                    cls._test_get_success, visitor,
                    elements=("head_lastlog", "col_lastlog"))

    def _test_get_success(self, visitor, elements):
        client = self.make_test_client(visitor)
        response = client.get(reverse("public_stats"))
        self.assertEqual(response.status_code, 200)
        self.assertContainsElements(response, self.elements, *elements)


class TestNewnmPermissions(PersonFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        # Everyone can see stats
        for visitor in (
                None, "pending", "dc", "dc_ga", "dm", "dm_ga", "dd_nu", "dd_u",
                "dd_e", "dd_r", "oldam", "activeam", "fd", "dam"):
            cls._add_method(cls._test_get_success, visitor)

    def _test_get_success(self, visitor):
        client = self.make_test_client(visitor)
        response = client.get(reverse("public_newnm"))
        self.assertEqual(response.status_code, 200)


class TestFindpersonPermissions(PersonFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for visitor in (
                None, "pending", "dc", "dc_ga", "dm", "dm_ga", "dd_nu", "dd_u",
                "dd_e", "dd_r", "oldam", "activeam"):
            cls._add_method(cls._test_get_success, visitor, elements=())
        for visitor in ("fd", "dam"):
            cls._add_method(cls._test_get_success, visitor, elements=("id_fd_comment",))

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.elements = PageElements()
        cls.elements.add_id("id_username")
        cls.elements.add_id("id_fd_comment")

    def _test_get_success(self, visitor, elements):
        client = self.make_test_client(visitor)
        response = client.get(reverse("public_findperson"))
        self.assertEqual(response.status_code, 200)
        self.assertContainsElements(response, self.elements, *elements)


class TestPeoplePermissions(PersonFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        # Everyone can see stats
        for visitor in (
                None, "pending", "dc", "dc_ga", "dm", "dm_ga", "dd_nu", "dd_u",
                "dd_e", "dd_r", "oldam", "activeam", "fd", "dam"):
            cls._add_method(cls._test_get_success, visitor)

    def _test_get_success(self, visitor):
        client = self.make_test_client(visitor)
        response = client.get(reverse("people"))
        self.assertEqual(response.status_code, 200)


class TestMembersPermissions(PersonFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        # Everyone can see stats
        for visitor in (
                None, "pending", "dc", "dc_ga", "dm", "dm_ga", "dd_nu", "dd_u",
                "dd_e", "dd_r", "oldam", "activeam", "fd", "dam"):
            cls._add_method(cls._test_get_success, visitor)

    def _test_get_success(self, visitor):
        client = self.make_test_client(visitor)
        response = client.get(reverse("members"))
        self.assertEqual(response.status_code, 200)


class TestManagersPermissions(PersonFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.elements = PageElements()
        cls.elements.add_th("cur", "Current")
        cls.elements.add_th("max", "Max")
        cls.elements.add_th("hold", "Hold")
        cls.elements.add_th("done", "Done")

    @classmethod
    def __add_extra_tests__(cls):
        for visitor in (
                None, "pending", "dc", "dc_ga", "dm", "dm_ga", "dd_nu", "dd_u",
                "dd_e", "dd_r", "oldam"):
            cls._add_method(cls._test_get_success, visitor, elements=())
        for visitor in ("activeam", "fd", "dam"):
            cls._add_method(cls._test_get_success, visitor, elements=("cur", "max", "hold", "done"))

    def _test_get_success(self, visitor, elements):
        client = self.make_test_client(visitor)
        response = client.get(reverse("managers"))
        self.assertEqual(response.status_code, 200)
        self.assertContainsElements(response, self.elements, *elements)
