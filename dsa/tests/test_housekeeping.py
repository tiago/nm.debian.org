from django.test import TestCase
from django.utils.timezone import now
from process.unittest import ProcessFixtureMixin
from dsa.housekeeping import CheckLDAPConsistency
from backend.models import Person
import backend.const as const
from process.models import Process
from sitechecks.models import Inconsistency
from unittest import mock


class MockHousekeeper:
    def __init__(self, housekeeper):
        self.housekeeper = type("Housekeeper", (object, ), {"user": housekeeper})

    def link(self, person):
        return str(person.ldap_fields.uid or person.pk)

    def run(self, cls, stage: str = "main"):
        task = cls(self)
        task.IDENTIFIER = f"test.{cls.__name__}"
        getattr(task, f"run_{stage}")(None)
        return task


class MockEntry:
    def __init__(self, uid, **attrs):
        attrs["uid"] = uid
        self.attrs = attrs
        self.uid = uid
        self.is_dd = "Debian" in self.attrs.get("supplementaryGid", ())
        self.is_guest = "guest" in self.attrs.get("supplementaryGid", ())

    def single(self, name):
        return self.attrs.get(name)


class TestCheckLDAPConsistency(ProcessFixtureMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.hk = MockHousekeeper(self.persons.oldam)

    def assertInconsistenciesEqual(self, items):
        res = list(Inconsistency.objects.all())
        self.assertEqual(len(res), len(items))
        for inc, item in zip(res, items):
            if "person" in item:
                self.assertEqual(inc.person, item["person"])
            if "text" in item:
                self.assertEqual(inc.text, item["text"])

    def test_new_removed(self):
        with mock.patch("dsa.udldap.list_people") as m:
            m.return_value = [
                MockEntry("newdd", supplementaryGid=["Debian"], cn="tcn",
                          mn="tmn", sn="tsn", emailForward="test@example.org", accountStatus=None)
            ]
            with self.assertLogs() as log:
                self.hk.run(CheckLDAPConsistency)
            self.assertEqual(log.output, [
                "WARNING:dsa.housekeeping:test.CheckLDAPConsistency:"
                " newdd: created to mirror a removed DD account from LDAP",
            ])

            p = Person.objects.get(ldap_fields__uid="newdd")
            audit = ["{}:{}".format(l.author.lookup_key, l.notes) for l in p.audit_log.all()]
            self.assertIn("oldam:created to mirror a removed DD account from LDAP", audit)
            self.assertEqual(len(audit), 2)
            self.assertEqual(p.ldap_fields.cn, "tcn")
            self.assertEqual(p.ldap_fields.mn, "tmn")
            self.assertEqual(p.ldap_fields.sn, "tsn")
            self.assertEqual(p.ldap_fields.email, "test@example.org")
            self.assertEqual(p.ldap_fields.uid, "newdd")
            self.assertEqual(p.email, "test@example.org")
            self.assertIsNone(p.fpr)
            self.assertFalse(p.identities.filter(issuer="debsso").exists())

    def test_new_guest(self):
        with mock.patch("dsa.udldap.list_people") as m:
            m.return_value = [
                MockEntry("newguest", supplementaryGid=[], cn="tcn", mn="tmn",
                          sn="tsn", emailForward="test@example.org", accountStatus=None)
            ]
            with self.assertLogs() as log:
                self.hk.run(CheckLDAPConsistency)
            self.assertEqual(log.output, [
                "WARNING:dsa.housekeeping:test.CheckLDAPConsistency:"
                " newguest: created to mirror a removed guest account from LDAP"])

            p = Person.objects.get(ldap_fields__uid="newguest")
            audit = ["{}:{}".format(l.author.lookup_key, l.notes) for l in p.audit_log.all()]
            self.assertIn("oldam:created to mirror a removed guest account from LDAP", audit)
            self.assertEqual(len(audit), 2)
            self.assertEqual(p.ldap_fields.cn, "tcn")
            self.assertEqual(p.ldap_fields.mn, "tmn")
            self.assertEqual(p.ldap_fields.sn, "tsn")
            self.assertEqual(p.ldap_fields.email, "test@example.org")
            self.assertEqual(p.ldap_fields.uid, "newguest")
            self.assertEqual(p.email, "test@example.org")
            self.assertIsNone(p.fpr)
            self.assertFalse(p.identities.filter(issuer="debsso").exists())

    def test_new_with_fpr(self):
        with mock.patch("dsa.udldap.list_people") as m:
            m.return_value = [
                MockEntry("newdd", gidNumber=800, supplementaryGid=["Debian"], cn="tcn", mn="tmn", sn="tsn",
                          emailForward="test@example.org", keyFingerPrint="66B4DFB68CB24EBBD8650BC4F4B4B0CC797EBFAB",
                          accountStatus=None)
            ]
            self.hk.run(CheckLDAPConsistency)
            self.assertInconsistenciesEqual([
                {"person": None,
                 "text": "newdd has fingerprint 66B4DFB68CB24EBBD8650BC4F4B4B0CC797EBFAB and gid 800 in LDAP,"
                         " but is not in our db"},
            ])

            with self.assertRaises(Person.DoesNotExist):
                Person.objects.get(ldap_fields__uid="newdd")

    def test_existing_match_uid(self):
        with mock.patch("dsa.udldap.list_people") as m:
            m.return_value = [
                MockEntry("dd_u", supplementaryGid=["Debian"], cn="tcn",
                          mn="tmn", sn="tsn", emailForward="test@example.org", accountStatus=None)
            ]
            with self.assertLogs() as log:
                self.hk.run(CheckLDAPConsistency)
            self.assertEqual(log.output, [
                "INFO:dsa.housekeeping:test.CheckLDAPConsistency: dd_u changing email_ldap from dd_u@example.org"
                " to test@example.org (source: LDAP)",
                'INFO:dsa.housekeeping:test.CheckLDAPConsistency: dd_u changing cn from Dd_u to tcn (source: LDAP)',
                'INFO:dsa.housekeeping:test.CheckLDAPConsistency: dd_u changing mn from  to tmn (source: LDAP)',
                'INFO:dsa.housekeeping:test.CheckLDAPConsistency: dd_u changing sn from  to tsn (source: LDAP)',
            ])

            p = self.persons.dd_u
            p.refresh_from_db()
            audit = ["{}:{}".format(l.author.lookup_key, l.notes) for l in p.audit_log.all()]
            self.assertIn("oldam:updated ldap_fields.email from LDAP", audit)
            self.assertIn("oldam:updated ldap_fields.cn from LDAP", audit)
            self.assertIn("oldam:updated ldap_fields.mn from LDAP", audit)
            self.assertIn("oldam:updated ldap_fields.sn from LDAP", audit)
            self.assertEqual(len(audit), 4)
            self.assertEqual(p.ldap_fields.cn, "tcn")
            self.assertEqual(p.ldap_fields.mn, "tmn")
            self.assertEqual(p.ldap_fields.sn, "tsn")
            self.assertEqual(p.email, "dd_u@example.org")
            self.assertEqual(p.ldap_fields.email, "test@example.org")

    def test_unexpected_dd(self):
        with mock.patch("dsa.udldap.list_people") as m:
            m.return_value = [
                MockEntry("dm", supplementaryGid=["Debian"], cn="Dm", mn="", sn="", emailForward="dm@example.org",
                          keyFingerPrint="66B4DFB68CB24EBBD8650BC4F4B4B0CC797EBFAB", accountStatus=None)
            ]
            self.hk.run(CheckLDAPConsistency)
            self.assertInconsistenciesEqual([
                {"person": self.persons.dm,
                 "text": "person has supplementaryGid 'Debian', but in our db the state is Debian Maintainer"},
            ])

            p = self.persons.dm
            p.refresh_from_db()
            self.assertEqual(p.audit_log.count(), 0)

    def test_unexpected_inactive(self):
        with mock.patch("dsa.udldap.list_people") as m:
            m.return_value = [
                MockEntry("dd_u", supplementaryGid=["Debian"], cn="Dd_u", mn="", sn="",
                          emailForward="dd_u@example.org", keyFingerPrint="66B4DFB68CB24EBBD8650BC4F4B4B0CC797EBFAB",
                          accountStatus="inactive 2018-03-20"),
            ]
            self.hk.run(CheckLDAPConsistency)

            self.assertInconsistenciesEqual([
                {"person": self.persons.dd_u,
                 "text":
                    "person has accountStatus 'inactive 2018-03-20' (comment: None)"
                    " but in our db the state is Debian Developer, uploading [inactive]"}
            ])

            p = self.persons.dd_u
            p.refresh_from_db()
            self.assertEqual(p.audit_log.count(), 0)

    def test_unexpected_inactive_with_comment(self):
        self.maxDiff = None
        with mock.patch("dsa.udldap.list_people") as m:
            m.return_value = [
                MockEntry("dd_u", supplementaryGid=["Debian"], cn="Dd_u", mn="", sn="", emailForward="dd_u@example.org",
                          keyFingerPrint="66B4DFB68CB24EBBD8650BC4F4B4B0CC797EBFAB",
                          accountStatus="inactive 2018-03-20", accountComment="RT#1234"),
            ]

            self.hk.run(CheckLDAPConsistency)
            self.assertInconsistenciesEqual([
                {"person": self.persons.dd_u,
                 "text":
                    "person has accountStatus 'inactive 2018-03-20' "
                    '(comment: RT#1234) but in our db the state is Debian Developer, uploading [inactive]'}
            ])

            p = self.persons.dd_u
            p.refresh_from_db()
            self.assertEqual(p.audit_log.count(), 0)

    def test_close_emeritus_process(self):
        with mock.patch("dsa.udldap.list_people") as m:
            process = Process.objects.create(person=self.persons.dd_u, applying_for=const.STATUS_EMERITUS_DD)

            m.return_value = [
                MockEntry("dd_u", supplementaryGid=["Debian"], cn="Dd_u", mn="", sn="",
                          emailForward="dd_u@example.org", accountStatus="retiring 2018-03-20"),
            ]
            self.hk.run(CheckLDAPConsistency)
            self.assertInconsistenciesEqual([
                {"person": self.persons.dd_u,
                 "text": "person has accountStatus 'retiring 2018-03-20' "
                 '(comment: None) but in our db the state is Debian Developer, uploading '
                 '[retiring]'},
            ])

            process.approved_by = self.persons.dam
            process.approved_time = now()
            process.save()
            with self.assertLogs() as log:
                self.hk.run(CheckLDAPConsistency)
            self.assertEqual(log.output, [
                "INFO:dsa.housekeeping:test.CheckLDAPConsistency: dd_u closed from dsa: retiring 2018-03-20",
            ])

            p = self.persons.dd_u
            p.refresh_from_db()
            audit = ["{}:{}".format(l.author.lookup_key, l.notes) for l in p.audit_log.all()]
            self.assertIn("oldam:closed from dsa: retiring 2018-03-20", audit)
            self.assertEqual(p.audit_log.count(), 1)
            self.assertEqual(p.status, const.STATUS_EMERITUS_DD)
            process.refresh_from_db()
            self.assertEqual(process.closed_by, self.persons.oldam)
            self.assertIsNotNone(process.closed_time)

    def test_close_emeritus_process_and_change_email(self):
        with mock.patch("dsa.udldap.list_people") as m:
            process = Process.objects.create(person=self.persons.dd_u, applying_for=const.STATUS_EMERITUS_DD)

            m.return_value = [
                MockEntry("dd_u", supplementaryGid=["Debian"], cn="Dd_u", mn="", sn="",
                          emailForward="test@example.org", accountStatus="retiring 2018-03-20"),
            ]
            with self.assertLogs() as log:
                self.hk.run(CheckLDAPConsistency)
            self.assertEqual(log.output, [
                "INFO:dsa.housekeeping:test.CheckLDAPConsistency: dd_u changing email_ldap"
                " from dd_u@example.org to test@example.org (source: LDAP)",
            ])
            self.assertInconsistenciesEqual([
                {"person": self.persons.dd_u,
                 "text": "person has accountStatus 'retiring 2018-03-20' "
                 '(comment: None) but in our db the state is Debian Developer, uploading '
                 '[retiring]'},
            ])

            process.approved_by = self.persons.dam
            process.approved_time = now()
            process.save()
            with self.assertLogs() as log:
                self.hk.run(CheckLDAPConsistency)
            self.assertEqual(log.output, [
                "INFO:dsa.housekeeping:test.CheckLDAPConsistency: dd_u closed from dsa: retiring 2018-03-20",
            ])

            p = self.persons.dd_u
            p.refresh_from_db()
            audit = ["{}:{}".format(l.author.lookup_key, l.notes) for l in p.audit_log.all()]
            self.assertIn("oldam:closed from dsa: retiring 2018-03-20", audit)
            self.assertIn("oldam:updated ldap_fields.email from LDAP", audit)
            self.assertEqual(p.audit_log.count(), 2)
            self.assertEqual(p.status, const.STATUS_EMERITUS_DD)
            self.assertEqual(p.ldap_fields.email, "test@example.org")
            process.refresh_from_db()
            self.assertEqual(process.closed_by, self.persons.oldam)
            self.assertIsNotNone(process.closed_time)
