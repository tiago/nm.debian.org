from __future__ import annotations
from contextlib import contextmanager
import tempfile
from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import now
from backend import const
from backend.email import get_mbox
from process.unittest import (
                     ProcessFixtureMixin,
                     test_fingerprint1, test_fpr1_signed_valid_text, test_fpr1_signed_valid_text_nonascii,
                     test_fingerprint2, test_fpr2_signed_valid_text,
                     test_fingerprint3, test_fpr3_signed_valid_text)
from process import ops as pops


class TestDownloadStatements(ProcessFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestDownloadStatements, cls).setUpClass()
        cls.create_person("app", status=const.STATUS_DC)
        cls.processes.create("app", person=cls.persons.app, applying_for=const.STATUS_DD_U, fd_comment="test")
        cls.processes.create(
                "ddem", person=cls.persons.dd_u, applying_for=const.STATUS_EMERITUS_DD, fd_comment="test_emeritus")
        cls.create_person("am", status=const.STATUS_DD_NU)
        cls.ams.create("am", person=cls.persons.am)
        cls.amassignments.create(
                "am", process=cls.processes.app, am=cls.ams.am, assigned_by=cls.persons["fd"], assigned_time=now())

        cls.visitor = cls.persons.dc

        cls.fingerprints.create(
                "app", person=cls.persons.app, fpr=test_fingerprint1, is_active=True, audit_skip=True)
        cls.fingerprints.create(
                "dd_nu", person=cls.persons.dd_nu, fpr=test_fingerprint2, is_active=True, audit_skip=True)
        cls.fingerprints.create(
                "dd_u", person=cls.persons.dd_u, fpr=test_fingerprint3, is_active=True, audit_skip=True)

        cls.statements.create("intent",
                              requirement=cls.processes.app.requirements.get(type="intent"),
                              fpr=cls.fingerprints.app,
                              statement=test_fpr1_signed_valid_text,
                              uploaded_by=cls.persons.app, uploaded_time=now())
        cls.statements.create("sc_dmup",
                              requirement=cls.processes.app.requirements.get(type="sc_dmup"),
                              fpr=cls.fingerprints.app,
                              statement=test_fpr1_signed_valid_text_nonascii,
                              uploaded_by=cls.persons.app, uploaded_time=now())

        cls.statements.create("intent_ddem",
                              requirement=cls.processes.ddem.requirements.get(type="intent"),
                              fpr=cls.fingerprints.dd_u,
                              statement=test_fpr3_signed_valid_text,
                              uploaded_by=cls.persons.dd_u, uploaded_time=now())
        # Python2's mbox seems to explode on non-ascii in headers
        cls.persons.dd_nu.cn = "Ondřej"
        cls.persons.dd_nu.sn = "Nový"
        cls.persons.dd_nu.save(audit_skip=True)
        cls.statements.create("advocate",
                              requirement=cls.processes.app.requirements.get(type="advocate"),
                              fpr=cls.fingerprints.dd_nu,
                              statement=test_fpr2_signed_valid_text,
                              uploaded_by=cls.persons.dd_nu,
                              uploaded_time=now())

    def test_backend(self):
        mbox_data = self.processes.app.get_statements_as_mbox(self.persons.fd)
        with tempfile.NamedTemporaryFile() as tf:
            tf.write(mbox_data)
            tf.flush()
            with get_mbox(tf.name) as mbox:
                self.assertEqual(len(mbox), 3)

    @contextmanager
    def response_mbox(self, response):
        self.assertEqual(response.status_code, 200)
        with tempfile.NamedTemporaryFile() as tf:
            tf.write(response.content)
            tf.flush()
            with get_mbox(tf.name) as mbox:
                yield mbox

    def test_download(self):
        url = reverse("process_download_statements", args=[self.processes.app.pk])
        client = self.make_test_client(self.visitor)
        response = client.get(url)
        with self.response_mbox(response) as mbox:
            self.assertEqual([m["Subject"] for m in mbox], [
                '=?utf-8?q?Signed_statement_for_Advocate?=',
                '=?utf-8?q?Signed_statement_for_Declaration_of_intent?=',
                '=?utf-8?q?Signed_statement_for_SC/DFSG/DMUP_agreement?='
            ])

    def test_download_emeritus_dc(self):
        url = reverse("process_download_statements", args=[self.processes.ddem.pk])
        client = self.make_test_client(self.persons.dc)
        response = client.get(url)
        with self.response_mbox(response) as mbox:
            self.assertEqual([m["Subject"] for m in mbox], [
            ])

    def test_download_emeritus_self(self):
        url = reverse("process_download_statements", args=[self.processes.ddem.pk])
        client = self.make_test_client(self.persons.dd_u)
        response = client.get(url)
        with self.response_mbox(response) as mbox:
            self.assertEqual([m["Subject"] for m in mbox], [
                '=?utf-8?q?Signed_statement_for_Declaration_of_intent?=',
            ])

    def test_download_emeritus_fd(self):
        url = reverse("process_download_statements", args=[self.processes.ddem.pk])
        client = self.make_test_client(self.persons.fd)
        response = client.get(url)
        with self.response_mbox(response) as mbox:
            self.assertEqual([m["Subject"] for m in mbox], [
                '=?utf-8?q?Signed_statement_for_Declaration_of_intent?=',
            ])

    def test_download_emeritus_dd(self):
        url = reverse("process_download_statements", args=[self.processes.ddem.pk])
        client = self.make_test_client(self.persons.dd_nu)
        response = client.get(url)
        with self.response_mbox(response) as mbox:
            self.assertEqual([m["Subject"] for m in mbox], [
                '=?utf-8?q?Signed_statement_for_Declaration_of_intent?=',
            ])


class TestViewsReqApproval(ProcessFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestViewsReqApproval, cls).setUpClass()
        cls.create_person("am", status=const.STATUS_DD_U)
        cls.ams.create("am", person=cls.persons.am)

        cls.create_person("app", status=const.STATUS_DC)
        cls.create_person("app2", status=const.STATUS_DM)
        cls.processes.create("dcga", person=cls.persons.app, applying_for=const.STATUS_DC_GA, fd_comment="testdcga")
        cls.processes.create("dm", person=cls.persons.app, applying_for=const.STATUS_DM, fd_comment="testDM")
        cls.processes.create("dmga", person=cls.persons.app2, applying_for=const.STATUS_DM_GA, fd_comment="testDMGA")
        cls.processes.create("ddu", person=cls.persons.app, applying_for=const.STATUS_DD_U, fd_comment="test")
        cls.processes.create("ddnu", person=cls.persons.app, applying_for=const.STATUS_DD_NU, fd_comment="test")
        cls.processes.create("dde", person=cls.persons.fd, applying_for=const.STATUS_EMERITUS_DD, fd_comment="test")
        cls.processes.create("ddr", person=cls.persons.dam, applying_for=const.STATUS_REMOVED_DD, fd_comment="test")

        cls.visitor = cls.persons.fd

        cls.fingerprints.create(
                "am", person=cls.persons.am, fpr=test_fingerprint1, is_active=True, audit_skip=True)
        cls.fingerprints.create(
                "fd", person=cls.persons.fd, fpr=test_fingerprint2, is_active=True, audit_skip=True)
        cls.fingerprints.create(
                "dam", person=cls.persons.dam, fpr=test_fingerprint3, is_active=True, audit_skip=True)

        cls.expected_ops = {
            "fd": [
                pops.ProcessApproveRT, pops.ProcessApproveRT, pops.ProcessApproveRT,
                pops.ProcessApprove, pops.ProcessApprove, pops.ProcessApproveRT, pops.ProcessApproveRT,
            ],
            "dam": [
                pops.ProcessApproveRT, pops.ProcessApproveRT, pops.ProcessApproveRT,
                pops.ProcessApproveRT, pops.ProcessApproveRT, pops.ProcessApproveRT, pops.ProcessApproveRT,
            ],
        }

        cls.process_list = ["dcga", "dm", "dmga", "ddu", "ddnu", "dde", "ddr"]

        cls.message = {
            "fd": test_fpr2_signed_valid_text,
            "dam": test_fpr3_signed_valid_text,
        }

    @classmethod
    def __add_extra_tests__(cls):
        for visitor in ["fd", "dam"]:
            for process in ["dcga", "dm", "dmga", "ddu", "ddnu", "dde"]:
                cls._add_method(cls._test_approval, visitor, process)

        cls._add_method(cls._test_approval, "dam", "ddr")

    def _test_approval(self, visitor, process):
        kwargs = {
            "pk": getattr(self.processes, process).pk,
            "type": "approval",
        }
        url = reverse("process_statement_create", kwargs=kwargs)
        client = self.make_test_client(visitor)
        with self.collect_operations() as ops:
            response = client.post(url, data={"statement": self.message[visitor]})
            req = self.processes.get(process).requirements.get(type="approval")
            self.assertRedirectMatches(response, req.process.get_absolute_url())
            self.assertEqual(response.status_code, 302)
            self.assertEqual(len(ops), 2)
            self.assertTrue(isinstance(ops[0], pops.ProcessStatementAdd))

            process_idx = self.process_list.index(process)
            self.assertTrue(isinstance(ops[1], self.expected_ops[visitor][process_idx]))

    def test_am_cant_approve_dd(self):
        kwargs = {
            "pk": self.processes.ddu.pk,
            "type": "approval",
        }
        url = reverse("process_statement_create", kwargs=kwargs)
        client = self.make_test_client(self.persons.am)
        with self.collect_operations() as ops:
            response = client.post(url, data={"statement": test_fpr1_signed_valid_text})
            self.assertEqual(response.status_code, 403)
            self.assertEqual(len(ops), 0)

    def test_am_cant_approve_dm(self):
        kwargs = {
            "pk": self.processes.dm.pk,
            "type": "approval",
        }
        url = reverse("process_statement_create", kwargs=kwargs)
        client = self.make_test_client(self.persons.am)
        with self.collect_operations() as ops:
            response = client.post(url, data={"statement": test_fpr1_signed_valid_text})
            self.assertEqual(response.status_code, 403)
            self.assertEqual(len(ops), 0)

    def test_fd_cant_approve_ddr(self):
        kwargs = {
            "pk": self.processes.ddr.pk,
            "type": "approval",
        }
        url = reverse("process_statement_create", kwargs=kwargs)
        client = self.make_test_client(self.persons.fd)
        with self.collect_operations() as ops:
            response = client.post(url, data={"statement": test_fpr2_signed_valid_text})
            self.assertEqual(response.status_code, 403)
            self.assertEqual(len(ops), 0)
