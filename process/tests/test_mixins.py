from django.test import TestCase
from backend import const
from process.unittest import ProcessFixtureMixin
from process.mixins import compute_process_status


class TestVisitApplicant(ProcessFixtureMixin, TestCase):
    def compute_perms(self, visited_status, visited_applying_for, visitor):
        self.create_person("app", status=visited_status)
        self.processes.create("app", person=self.persons.app, applying_for=visited_applying_for)
        self.processes.app.add_log(
            changed_by=self.persons.fd,
            logtext="test private",
            is_public=False,
        )
        self.processes.app.add_log(
            changed_by=self.persons.fd,
            logtext="test public",
            is_public=True,
        )
        visit_perms = self.persons.app.permissions_of(visitor)
        return compute_process_status(self.processes.app, visitor, visit_perms=visit_perms)

    def test_dm_dc(self):
        perms = self.compute_perms(const.STATUS_DC, const.STATUS_DC_GA, self.persons.dm)
        self.assertEqual(perms["log_first"].logtext, "test public")
        self.assertEqual(perms["log_last"].logtext, "test public")

    def test_fd_dc(self):
        perms = self.compute_perms(const.STATUS_DC, const.STATUS_DC_GA, self.persons.fd)
        self.assertEqual(perms["log_first"].logtext, "test private")
        self.assertEqual(perms["log_last"].logtext, "test public")
