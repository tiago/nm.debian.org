(function($){
"use strict";

const epoch = "1970-01-01T00:00:00+00:00";

function has_modifiers(evt)
{
    return evt.altKey || evt.ctrlKey || evt.isComposing || evt.metaKey || evt.shiftKey;
}

class MessageTable
{
    constructor(options)
    {
        this.options = options;
        this.mbox = options.mbox;
        this.el_table = document.getElementById(options.table);
        this.el_tbody = $(this.el_table).find("tbody")[0];
        this.table = $(this.el_table).find("table");

        this.el_tbody.addEventListener("keydown", evt => {
            // Skip all events with modifiers, since we don't use any, and this
            // way we prevent eating browser shortcuts
            if (has_modifiers(evt))
                return;
            const code = evt.code;
            let ignored = false;
            if (code == "Escape")
                this.quit_table();
            else if (code == "Enter")
                this.select_message(this.current_message);
            else if (code == "ArrowUp" || code == "ArrowLeft")
                this.prev_message();
            else if (code == "ArrowDown" || code == "ArrowRight")
                this.next_message();
            else if (code == "PageUp")
                this.prev_message(10);
            else if (code == "PageDown")
                this.next_message(10);
            else if (code == "Home")
                this.first_message();
            else if (code == "End")
                this.last_message();
            else
                ignored = true;
            if (!ignored)
                evt.preventDefault();
        });

        this.el_tbody.addEventListener("wheel", evt => {
            // Skip all events with modifiers, since we don't use any so far
            if (has_modifiers(evt))
                return;
            if (evt.deltaY < 0)
                this.prev_message();
            else
                this.next_message();
        });

        this.el_tbody.addEventListener("click", evt => {
            // Skip all events with modifiers, since we don't use any so far
            if (has_modifiers(evt))
                return;
            // Find the row clicked
            let tr = evt.target;
            while (tr && tr.tagName != "TR")
                tr = tr.parentElement;
            if (!tr)
                return;

            // Find the index of the row
            let rows = this.el_tbody.getElementsByTagName("tr");
            let idx = 0;
            for (let i = 0; i < rows.length; ++i)
                if (rows[i] == tr)
                {
                    idx = i;
                    break;
                }

            if (this.options.click_selects)
                this.select_message(idx);
            else
                this.focus_message(idx);
        });

        let tbody = this.table.find("tbody");
        for (const msg of this.mbox)
        {
            let tr = $("<tr>");
            let time = $("<time>").attr("datetime", msg.date_iso ? msg.date_iso  : epoch).text(msg.date_rfc);
            tr.append($("<td>").append(time));
            tr.append($("<td>").text(msg.from));
            tr.append($("<td>").text(msg.subject));
            tbody.append(tr); 
        }

        this.current_message = undefined;
        this.focus_message(0);
    }

    quit_table()
    {
        if (this.options.on_quit)
            this.options.on_quit();
    }

    select_message(idx)
    {
        this.focus_message(idx);
        if (this.options.on_select)
            this.options.on_select(idx);
    }

    focus_message(idx)
    {
        if (idx === undefined)
            idx = this.current_message;
        let rows = this.el_tbody.getElementsByTagName("tr");
        for (let i = 0; i < rows.length; ++i)
        {
            let row = rows[i];
            if (i == idx)
            {
                row.classList.add("table-primary");
                row.scrollIntoView({
                    behaviour: "smooth",
                    block: "nearest",
                });
            } else {
                row.classList.remove("table-primary");
            }
        }
        this.current_message = idx;
        if (this.options.on_focus)
            this.options.on_focus(idx);
    }

    next_message(count=1)
    {
        if (this.current_message === undefined)
            this.focus_message(0);
        else
            this.focus_message(Math.min(this.mbox.length - 1, this.current_message + count));
    }

    prev_message(count=1)
    {
        if (this.current_message === undefined)
            this.focus_message(0);
        else
            this.focus_message(Math.max(0, this.current_message - count));
    }

    first_message()
    {
        this.focus_message(0);
    }

    last_message()
    {
        this.focus_message(this.mbox.length - 1);
    }
}

if (window.nm2 === undefined)
    window.nm2 = {};

window.nm2.MessageTable = MessageTable;

})(jQuery);
